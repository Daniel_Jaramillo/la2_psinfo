#ifndef OBTENCIONINFO_H
#define OBTENCIONINFO_H
#define NOMBRE "Name";
#define ESTAD0 "State";
#define TAMANIO_TOTAL "VmSize";
#define TAMANIO_TEXT "VmData";
#define TAMANIO_DATA "VmExe";
#define TAMANIO_STACK "VmStk";
#define CAMBIO_CTXT_VOLUNTARIOS "voluntary_ctxt_switches";
#define CAMBIO_CTXT_INVOLUNTARIOS "nonvoluntary_ctxt_switches";
#include <stdio.h>

char *obtenerDatosFilaN(FILE *archivo, int n);
int obtenerFilaN(FILE *archivo, char *clave);

#endif