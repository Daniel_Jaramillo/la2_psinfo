#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #define NOMBRE "Name";
// #define ESTAD0 "State";
// #define TAMANIO_TOTAL "VmSize";
// #define TAMANIO_TEXT "VmData";
// #define TAMANIO_DATA "VmExe";
// #define TAMANIO_STACK "VmStk";
// #define CAMBIO_CTXT_VOLUNTARIOS "voluntary_ctxt_switches";
// #define CAMBIO_CTXT_INVOLUNTARIOS "nonvoluntary_ctxt_switches";

/*
  método para obtener el los datos del proceso en la linea n
  Parámetros:
    *archivo: arhivo a leer (archivo status)
    n: linea de la que se quieren extraer los datos
  Retorno: puntero almacenando un vector de caracteres con la información deseada
*/
char *obtenerDatosFilaN(FILE *archivo, int n)
{

  rewind(archivo);

  char ch;

  /*
    Se avanza hasta la linea n del archivo
  */
  n--;
  while (n > 0)
  {
    ch = getc(archivo);
    if (ch == 10)
      n--;
  }

  do
  {
    ch = getc(archivo);
  } while (ch != 9);
  /*
    Hasta esta linea se tiene el "apuntador" de lectura de los caracteres (getc) en la primera letra del nombre del proceso. Es decir, el apuntador está después de que se hizo tab (caracter ASCII #9) en la linea n del archivo "status".
  */

  int i = 0;
  char *retorno = malloc(sizeof(char) * 100);
  do
  {
    ch = getc(archivo);
    *(retorno + i) = ch;
    i++;
  } while (ch != 10);
  //*(retorno + i) = '\0';
  /*
    Hasta esta linea se almacenó en *retorno el nombre del proceso (posiblemente sobrando muchos espacios).
    la variable 'i' almacena el número de elementos en *retorno (sin contar el caracter '\0')
  */

  retorno = realloc(retorno, i);
  *(retorno + i - 1) = '\0';
  return retorno;
}

/*
  método para obtener la fila donde se encutentra la clave que nosotros queremos (pensando en el concepto clave-valor)
  Parámetros:
    *archivo: arhivo a leer (archivo status)
    *clave: puntero que contiene la clave deseada
  Retorno: entero con la linea donde está la clave dentro del archivo status 
*/
int obtenerFilaN(FILE *archivo, char *clave)
{
  rewind(archivo);

  char ch;
  int flag = 1;
  int linea = 1;
  /*
    Se va a recorrer todo el archivo "status" del proceso en cuestión
  */
  while (ch != EOF)
  {
    ch = getc(archivo);
    int i = 0;
    /*
      Voy preguntando si los caracteres en *clave* y en *archivo* (en la linea número *linea*) son iguales.
      Cuando se encuentra que son diferentes, termina de comparar y pasa a la siguiente linea del archivo status.
    */
    while (ch == *(clave + i))
    {
      ch = getc(archivo);
      i++;
    }
    /*
      Se pregunta si tanto *ch* (que recorre *archivo) como hasta el caracter donde llegó *clave* son los caracteres "finales".
    */
    if (ch == ':' && *(clave + i)==0)
    {
      return linea;
    }
    i = 0; //necesario para poder comparar clave desde el primer caracter en cada iteración

    if (ch == 10)
      linea++;
  }
  return -1;
}