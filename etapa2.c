#include "obtencionInfo.h"
#include "Proceso.h"

typedef struct Proceso Proceso;
void imprimirListaProcesos(int numeroProceso, Proceso *proceso);
void generarReporte(int numeroProcesos, Proceso *ps, int argc, char *argv[]);
char *obtenerNombreArchivo(int argc, char *argv[]);
Proceso cargarInformacionProceso(Proceso *ps);

 /*
  Obtiene la lista de procesos.
*/
void obtenerListaProcesos(int argc, char *argv[], Proceso *ps){
  int numeroEntradas = argc - 1; //Numero de argumentos ingresados
  int numeroProcesos = argc - 2; //NUmero de procesos a consultar
  
  Proceso *p;

  int j = 0;
  for (int i = 2; i <= numeroEntradas; i++)
  {
    strcpy(ps[j].id, argv[i]);
    p = &ps[j];
    cargarInformacionProceso(p);
    j++;
  }
}

/**
Consulta y asigna a cada item del Vector proceso, la información asociada
a cada proceso.
Recibe como parametro un apuntador hacia el proceso en cuestion.(Ingresado por el usuario.)
**/
Proceso cargarInformacionProceso(Proceso *ps)
{
  char statusFilename[128];
  sprintf(statusFilename, "/proc/%s/status", ps->id);
  FILE *fd = fopen(statusFilename, "r");

  if (fd == NULL)
  {
    printf("Error al buscar proceso %s\n", ps->id);
    exit(-1);
  }

  int linea_deseada;
  char *datos;
  char *clave;

  clave = NOMBRE;
  linea_deseada = obtenerFilaN(fd, clave);
  datos = obtenerDatosFilaN(fd, linea_deseada);

  char *nombre = datos;

  clave = ESTAD0;
  linea_deseada = obtenerFilaN(fd, clave);
  datos = obtenerDatosFilaN(fd, linea_deseada);

  char *estado = datos;

  strcpy(ps->nombreProceso, nombre);
  strcpy(ps->estado, estado);
  fclose(fd);
}

/**
imprimi cada uno los item del vector proceso
con los datos consultados recientemente.
Argumentos:
numeros de procesos a analizar
Apuntador hacia el vector de procesos.
**/

void imprimirListaProcesos(int numeroProceso, Proceso *proceso)
{

  for (int i = 0; i < numeroProceso; i++)
  {
    printf("Pdi:%s", proceso[i].id);
    printf("\n");
    printf("Nombre del Proceso:%s", proceso[i].nombreProceso);
    printf("\n");
    printf("Estado:%s", proceso[i].estado);
    printf("\n");
    printf("\n");
  }
}

void generarReporte(int numeroProcesos, Proceso *proceso, int argc, char *argv[]) {
  printf("Generando reporte...");
  printf("\n");

  char *nombreArchivo = obtenerNombreArchivo(argc, argv);

  FILE *outFile;
  outFile=fopen(nombreArchivo,"w");

  for (int i = 0; i < numeroProcesos; i++)
  {
    fputs("Pdi: ", outFile);
    fputs(proceso[i].id, outFile);
    fputs("\n", outFile);

    fputs("Nombre del proceso: ", outFile);
    fputs(proceso[i].nombreProceso, outFile);
    fputs("\n", outFile);

    fputs("Estado: ", outFile);
    fputs(proceso[i].estado, outFile);
    fputs("\n", outFile);
    fputs("\n", outFile);
  }
  fclose(outFile);

  printf("Reporte generado exitosamente");
  printf("\n");
}

char *obtenerNombreArchivo(int argc, char *argv[]) {
  int size = 100;
  char *nombreArchivo = malloc(sizeof(char)*size);
  for (int i =0; i < size; i++){
    *(nombreArchivo+i) = '\0';
  }
  strcat(nombreArchivo,"psinfo-report");
  int i = 2;
  while(i<argc){
    strcat(nombreArchivo, "-");
    strcat(nombreArchivo, argv[i]);
    i++;
  }
  strcat(nombreArchivo, ".info");
  return nombreArchivo;
}
