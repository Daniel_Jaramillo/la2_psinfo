#include "obtencionInfo.h"

/*
  Función para imprimir el estado de un proceso
  PARAMETROS:
    char *argv[]: puntero con los argumentos pasados en ejecución.
  RETORNO:
    0 en caso de ejecutarse exitosamente, 1 en caso contrario
*/
int imprimir_estado_proceso(char *argv[])
{ 
  FILE *inputFile;
  char *id_proceso = argv[1];
  char ruta[50] = "";
  strcat(ruta, "/proc/");
  strcat(ruta, id_proceso);
  strcat(ruta, "/status");
  inputFile = fopen(ruta, "r");

  FILE *outFile;
  outFile = fopen("outFilname.txt", "w");

  if (inputFile == NULL)
  {
    printf("Error al abrir el archivo %s\n", argv[1]);
    exit(-1);
  }
  char *clave;
  
  clave = NOMBRE;
  imprimir_datos(inputFile, clave);
  
  clave = ESTAD0;
  imprimir_datos(inputFile, clave);

  clave = TAMANIO_TOTAL;
  imprimir_datos(inputFile, clave);

  clave = TAMANIO_TEXT;
  imprimir_datos(inputFile, clave);

  clave = TAMANIO_DATA;
  imprimir_datos(inputFile, clave);
  
  clave = TAMANIO_STACK;
  imprimir_datos(inputFile, clave);
  
  clave = CAMBIO_CTXT_VOLUNTARIOS;
  imprimir_datos(inputFile, clave);
  
  clave = CAMBIO_CTXT_INVOLUNTARIOS;
  imprimir_datos(inputFile, clave);
  
  
  fclose(inputFile);
  exit(0);
}

void imprimir_datos(FILE *archivo, char *clave)
{
  int linea_deseada;
  char *datos;

  linea_deseada = obtenerFilaN(archivo, clave);
  if (linea_deseada == -1){
    printf ("%s: Datos no encontrados", clave);
    printf("\n");
    return;
  }
  datos = obtenerDatosFilaN(archivo, linea_deseada);
  printf ("%s: %s", clave, datos);
  printf("\n");
}