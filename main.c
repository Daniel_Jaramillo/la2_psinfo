#include <stdio.h>
#include <string.h>
#include "Proceso.h"


typedef struct Proceso Proceso;

int main(int argc, char *argv[]) {

  int numeroEntradas = argc - 1; // número de argumentos ingresados
  int numeroProcesos = argc - 2; // número de procesos a consultar

  if (numeroEntradas == 0) {
    printf("Ejecute el programa con las instrucciones (argumentos) adecuadas");
    return 1;
  } else if (numeroEntradas == 1) {
    imprimir_estado_proceso(argv);
    return 0;
  }

  char *peticion = argv[1];
  int estadoPeticion;

  if (strlen(argv[1])!=2){
    printf("Instrucción %s no reconocida", argv[1]);
    printf("\n");
    return 1;
  }

  if (*(peticion+1) == 'l'){
    estadoPeticion = 1;
  } else if (*(peticion+1) == 'r') {
    estadoPeticion = 2;
  }
  if (*peticion != '-'){
    estadoPeticion = -1;
  }

  Proceso *ps;
  if (estadoPeticion == 1) {
    Proceso ps[numeroProcesos];
    obtenerListaProcesos(argc, argv, ps);
    imprimirListaProcesos(numeroProcesos, ps);
  } else if (estadoPeticion == 2) {
    Proceso ps[numeroProcesos];
    obtenerListaProcesos(argc, argv, ps);
    generarReporte(numeroProcesos, ps, argc, argv);
  } else {
    printf("%s: petición no reconocida", peticion);
    printf("\n");
  }

  return 0;
}